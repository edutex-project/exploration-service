from django.shortcuts import render
from channels_presence.models import Presence
from django.contrib.auth.decorators import login_required
from sessionhandler.models import Survey, Notification, RequestSensors, DatabaseSurvey, DatabaseNotification, \
    DatabaseRequestSensor, Study, Participant, MicroSurvey, MicroSurveyDatabase, Session
from core.extract_db import extract_by_study, extract_by_participant, extract_by_session
import json
from django.http import JsonResponse, HttpResponse
from django.template import loader
from django.contrib.auth import logout
from django.shortcuts import redirect
from datetime import datetime


@login_required(login_url="/login/")
def index(request, sessions=[], searched_param="", goTo=""):
    context = {}

    if request.user.has_perm('application.admin_access'):
        curr_presence = Presence.objects.all()
        presence = list(set([i.room.channel_name.split("_")[1] for i in curr_presence]))

        active_surveys = [i.name for i in Survey.objects.all()]
        active_notifications = [i.name for i in Notification.objects.all()]
        active_sensor_requests = [i.name for i in RequestSensors.objects.all()]
        active_micros = [i.name for i in MicroSurvey.objects.all()]

        last_surveys = [i.__str__() for i in DatabaseSurvey.objects.all().order_by("-id")[:10]]
        last_notifications = [i.__str__() for i in DatabaseNotification.objects.all().order_by("-id")[:10]]
        last_sensor_requests = [i.__str__() for i in DatabaseRequestSensor.objects.all().order_by("-id")[:10]]
        last_micros = [i.__str__() for i in
                       MicroSurveyDatabase.objects.all().order_by("-id")[:10]]

        # Accumulate Session Search Datas
        dict_session_search = {}
        for i in Study.objects.all():
            dict_session_search[i.name] = []

        for j in Participant.objects.all():
            dict_session_search[j.study.name].append((j.name, j.identifier))

        dict_search_list = []
        for i in dict_session_search.keys():
            dict_search_list.append((i, dict_session_search[i]))

        # Search used
        searched_sessions = []
        if sessions:
            for session in sessions:
                searched_param = (searched_param[0], searched_param[1], searched_param[2])
                q_study = Study.objects.filter(name=searched_param[0])
                if q_study:
                    if int(session.study_name) != q_study[0].id:
                        # Filtering Sessions which did take place during another study not included in the search terms.
                        continue

                num_sensor_requests = sum(
                    [1 for i in DatabaseRequestSensor.objects.filter(sessionID=session.sessionID)])
                num_micros = sum([1 for i in DatabaseSurvey.objects.filter(sessionID=session.sessionID)])
                num_surveys = sum([1 for i in DatabaseNotification.objects.filter(sessionID=session.sessionID)])
                num_notifications = sum([1 for i in MicroSurveyDatabase.objects.filter(sessionID=session.sessionID)])
                total_questionnaires = num_notifications + num_surveys + num_micros

                searched_sessions.append((session.sessionID, session.start, session.ending,
                                          num_sensor_requests, total_questionnaires))



        context = {
            'goToID': goTo,
            'is_admin': True,
            'active_studies': [i.name for i in Study.objects.all()],
            'participants': [(i.name, i.identifier) for i in Participant.objects.all()],
            'presence_room_ids': presence,
            'active_surveys': active_surveys,
            'active_notifications': active_notifications,
            'active_sensor_requests': active_sensor_requests,
            'active_micros': active_micros,
            'db_surveys': last_surveys,
            'db_micros': last_micros,
            'db_notification': last_notifications,
            'db_sensor_requests': last_sensor_requests,
            'session_search': str(json.dumps(dict_session_search)),
            'session_searched': searched_sessions,
            'searched_param': searched_param,

        }

    else:
        tmp_user = request.user
        query_participants = Participant.objects.filter(user=tmp_user)
        if len(query_participants) == 1:
            participant = query_participants[0]
            query_study = Study.objects.filter(participant=participant)
            if len(query_study) == 1:
                tmp_study = query_study[0]
                user_active_surveys = [i.name for i in Survey.objects.filter(study=tmp_study)]
                user_active_notifications = [i.name for i in Notification.objects.filter(study=tmp_study)]
                user_active_sensor_requests = [i.name for i in RequestSensors.objects.filter(study=tmp_study)]
                user_active_micros = [i.name for i in MicroSurvey.objects.filter(study=tmp_study)]

                user_db_surveys = [i.__str__() for i in
                                   DatabaseSurvey.objects.filter(userID=participant.identifier).order_by("-id")[:10]]
                user_db_notifications = [i.__str__() for i in
                                         DatabaseNotification.objects.filter(userID=participant.identifier).order_by(
                                             "-id")[:10]]
                user_db_sensor_requests = [i.__str__() for i in
                                           DatabaseRequestSensor.objects.filter(userID=participant.identifier).order_by(
                                               "-id")[:10]]
                user_db_micros = [i.__str__() for i in
                                  MicroSurveyDatabase.objects.filter(userID=participant.identifier).order_by("-id")[
                                  :10]]

                user_db_session = [(i.sessionID, i.start, i.ending) for i in
                                   Session.objects.filter(participant=participant)]

                num_questionnaires = len(user_db_micros) + len(user_db_notifications) + len(user_db_surveys)

                context = {
                    'goToID': goTo,
                    'participant_name': participant.name,
                    'participant_study': participant.study.name,
                    'participant_identifier': participant.identifier,
                    'participant_num_questionnaires': num_questionnaires,
                    'participant_num_sensorreq': len(user_db_sensor_requests),
                    'participant_restricted_data_usage': participant.restricted_usage,
                    'is_admin': False,
                    'active_surveys': user_active_surveys,
                    'active_notifications': user_active_notifications,
                    'active_sensor_requests': user_active_sensor_requests,
                    'active_micros': user_active_micros,
                    'db_surveys': user_db_surveys,
                    'db_notification': user_db_notifications,
                    'db_sensor_requests': user_db_sensor_requests,
                    'db_micros': user_db_micros,
                    'db_session': user_db_session,
                }
        if context == {}:
            context = {
                'goToID': goTo,
                'participant_name': "Unregistered",
                'participant_study': "Unregistered",
                'participant_identifier': "Unregistered",
                'participant_num_questionnaires': "Unregistered",
                'participant_num_sensorreq': "Unregistered",
                'participant_restricted_data_usage': True,
                'is_admin': False,
                'active_surveys': [],
                'active_notifications': [],
                'active_sensor_requests': [],
                'active_micros': [],
                'db_surveys': [],
                'db_notification': [],
                'db_sensor_requests': [],
                'db_micros': [],
                'db_session': [],
            }

    return render(request, 'core/index.html', context)


@login_required(login_url="/login/")
def request_study(request, study_name):
    time = str(datetime.utcnow().isoformat()[:-3]) + "Z"

    if request.user.has_perm('application.admin_access'):
        ret = extract_by_study(study_name, True)
        response = HttpResponse(ret, content_type='application/gzip')
        response['Content-Disposition'] = 'attachment; filename=' + study_name + "_" + time + '.json.gz'
        return response
    else:
        ret = {
            'study': study_name,
            'permission': 'denied'
        }
        response = HttpResponse(json.dumps(ret), content_type='application/json')
        response['Content-Disposition'] = 'attachment; filename=' + study_name + "_" + time + '.json'
        return response


@login_required(login_url="/login/")
def request_participant(request, participant_name):
    time = str(datetime.utcnow().isoformat()[:-3]) + "Z"

    if request.user.has_perm('application.admin_access'):
        ret = extract_by_participant(participant_name, True)
        response = HttpResponse(ret, content_type='application/gzip')
        response['Content-Disposition'] = 'attachment; filename=' + participant_name + "_" + time + '.json.gz'
        return response
    else:
        tmp_participant = Participant.objects.filter(user=request.user)
        if tmp_participant:
            ret = extract_by_participant(tmp_participant[0].identifier, True)
            response = HttpResponse(ret, content_type='application/gzip')
            response['Content-Disposition'] = 'attachment; filename=' + str(tmp_participant[0].name) + "_" + str(tmp_participant[0].study.name) + "_" + time + '.json.gz'
            return response
        else:
            return index(request)


@login_required(login_url="/login/")
def request_session(request, sessionID):
    if request.user.has_perm('application.admin_access'):
        time = str(datetime.utcnow().isoformat()[:-3]) + "Z"

        q_sessions = Session.objects.filter(sessionID=sessionID)
        print(q_sessions)
        if q_sessions:
            ret = extract_by_session(q_sessions[0].sessionID, True)
            response = HttpResponse(ret, content_type='application/gzip')
            response['Content-Disposition'] = 'attachment; filename=' + sessionID + "_" + time + '.json.gz'
            return response
        else:
            return index(request)
    else:
        return index(request)


@login_required(login_url="/login/")
def delete_by_session(request, sessionid):
    try:

        tmp_participant = Participant.objects.filter(user=request.user)[0]
        query_session = Session.objects.filter(participant=tmp_participant, sessionID=sessionid)
        print(query_session[0])

        if query_session:
            tmp_session = query_session[0]
            sessionID = tmp_session.sessionID

            DatabaseNotification.objects.filter(sessionID=sessionID).delete()
            DatabaseSurvey.objects.filter(sessionID=sessionID).delete()
            MicroSurveyDatabase.objects.filter(sessionID=sessionID).delete()
            DatabaseRequestSensor.objects.filter(sessionID=sessionID).delete()
            tmp_session.delete()
    except:
        pass

    return index(request)


@login_required(login_url="/login/")
def delete_by_studyname(request, studyname):
    try:
        tmp_participant = Participant.objects.filter(user=request.user)[0]

        query_study = Study.objects.filter(name=studyname)[0]
        study_identifier = query_study.getIdentifier()

        query_session = Session.objects.filter(participant=tmp_participant, study_name=study_identifier)
        for session in query_session:
            sessionID = session.sessionID
            DatabaseNotification.objects.filter(sessionID=sessionID).delete()
            DatabaseSurvey.objects.filter(sessionID=sessionID).delete()
            MicroSurveyDatabase.objects.filter(sessionID=sessionID).delete()
            DatabaseRequestSensor.objects.filter(sessionID=sessionID).delete()
            session.delete()
    except:
        pass

    return index(request)


@login_required(login_url="/login/")
def toggle_data_permission(request):
    try:
        tmp_participant = Participant.objects.filter(user=request.user)[0]
        tmp_participant.restricted_usage = not tmp_participant.restricted_usage
        tmp_participant.save()
    except Exception as er:
        print(er)
    return index(request)


@login_required(login_url="/login/")
def delete_user(request):
    try:
        tmp_participant = Participant.objects.filter(user=request.user)[0]
        query_session = Session.objects.filter(participant=tmp_participant)
        for session in query_session:
            sessionID = session.sessionID
            DatabaseNotification.objects.filter(sessionID=sessionID).delete()
            DatabaseSurvey.objects.filter(sessionID=sessionID).delete()
            MicroSurveyDatabase.objects.filter(sessionID=sessionID).delete()
            DatabaseRequestSensor.objects.filter(sessionID=sessionID).delete()
            session.delete()

        tmp_participant.delete()

        print(request.user)
        user = request.user
        logout(request)
        user.delete()
        print(request.user)

    except:
        print(request.user)
        user = request.user
        logout(request)
        user.delete()
        print(request.user)

    return redirect("../login/")


@login_required(login_url="/login/")
def update_session_extract(request, studyname, participant_name):
    print("Inside Function")
    print(request)
    if request.user.has_perm('application.admin_access'):
        participant = Participant.objects.filter(identifier=participant_name)
        if len(participant) > 0:
            sessions = list(Session.objects.filter(participant=participant[0]))
            return index(request, sessions=sessions, searched_param=(studyname, participant[0].name, participant_name),
                         goTo="searched_session_container")
        else:
            sessions = []
            return index(request, goTo="searched_session_container")

    else:
        return index(request)
