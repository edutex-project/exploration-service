from sessionhandler.models import *
import json
import traceback

def format_user_answers(answer, poss_answers):

    numeric_answers_bool = False
    numeric_answers = sum([1 for i in poss_answers if i[1].isdigit()])
    if numeric_answers == len(poss_answers):
        numeric_answers_bool = True

    answer_digit = answer.isdigit()


    if answer_digit and numeric_answers_bool:
        return list(filter(lambda x: str(x[0]) == str(answer), poss_answers))
    elif answer_digit and not numeric_answers_bool:
        return list(filter(lambda x: str(x[0]) == str(answer), poss_answers))
    elif not answer_digit and not numeric_answers_bool:
        return list(filter(lambda x: str(x[1]) == str(answer), poss_answers))
    elif not answer_digit and numeric_answers_bool:
        return []


def format_timestamps_asked(timestamps):
    if timestamps == "Error not received correct Key.":
        return []

    new_timestamps = []
    for concatenated in timestamps:
        tmp = concatenated.split(";")
        try:
            new_timestamps.append({"timestamp":tmp[0]})
        except:
            pass

    return new_timestamps


def format_sensor_request(sensor_req):
    default = {
        'id': int(sensor_req.sensorrequest_id),
        #'name': sensor_req.name,
        'sensor': sensor_req.sensor,
        #'type': sensor_req.type,
        #'schedule': int(sensor_req.schedule),
        #'timestamp_requested': sensor_req.timestamp_requested,
    }

    used_sensor = sensor_req.sensor
    try:
        values = sensor_req.values

        datasource = ""
        try:
            datasource = values["datasource"]
        except:
            pass

        my_value = {
            "sensor_event_data_specification": values["sensor_event_data_specification"],
            "sensor_type": values["sensor_type"],
            "datasource": datasource,
        }

        if used_sensor in ["accelerometer", "bluetooth","wifi", "gps"]:
            events = values["events"]
            my_value["effective_time_start"] = events[0]["effective_time_frame"]["date_time"]
            my_value["effective_time_end"] = events[len(events) - 1]["effective_time_frame"]["date_time"]
            my_value["events"] = events

        elif used_sensor in ["heart_rate", "microphone", "ambientlight", "temperature", "sdnn",
                             "oxygen_saturation"]:

            tmp = ""
            if used_sensor == "heart_rate": tmp = "heart_rate"
            if used_sensor == "microphone": tmp = "noise level"
            if used_sensor == "ambientlight": tmp = "ambientlight"
            if used_sensor == "sdnn": tmp = "sdnn"
            if used_sensor == "oxygen_saturation": tmp = "oxygen_saturation"
            if used_sensor == "temperature": tmp= "temperature"

            events = values["events"]
            my_value["effective_time_start"] = events[0]["effective_time_frame"]["date_time"]
            my_value["effective_time_end"] = events[len(events) - 1]["effective_time_frame"]["date_time"]
            my_value["min_recorded_value"] = min([i[tmp]["value"] for i in events])
            my_value["max_recorded_value"] = max([i[tmp]["value"] for i in events])
            my_value["events"] = events

        default["value"] = my_value
        return default
    except Exception as er:
        print(er)
        default["value"] = []
        return default


def format_answers_to_dic(answers):
    tmp = []
    try:
        for i in answers:
            options_type = "String"
            if str(i[1]).isdigit():
                options_type = "Integer"

            tmp.append({
                "index": i[0],
                "value": i[1],
                "type": options_type,
            })
    except:
        pass

    return tmp