from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.forms.utils import ErrorList
from django.http import HttpResponse
from .forms import LoginForm, SignUpForm
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth import get_user_model
from rest_framework import generics
from rest_framework_simplejwt.views import TokenObtainPairView  # new
from .serializers import LogInSerializer, UserSerializer  # changed


# Create your views here.
def login_view(request):
    form = LoginForm(request.POST or None)

    msg = None
    submitted_once = False

    if request.method == "POST":
        submitted_once = True
        if form.is_valid():
            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password")
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("/")
            else:
                msg = 'Ungültige Anmeldedaten'
        else:
            msg = 'Error validating the form'

    return render(request, "authentification/login.html",
                  {"form": form, "msg": msg, "submitted_already": submitted_once, })


def register_user(request):
    msg = None
    success = False
    submitted_once = False
    if request.method == "POST":
        submitted_once = True
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get("username")
            raw_password = form.cleaned_data.get("password1")
            user = authenticate(username=username, password=raw_password)
            msg = 'User created.'
            success = True
            # return redirect("/login/")
        else:
            msg = '  '
    else:
        form = SignUpForm()

    return render(request, "authentification/register.html",
                  {"form": form, "msg": msg, "success": success, "submitted_already": submitted_once, })


def logout_user(request):
    logout(request)
    return login_view(request)


class HelloView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        content = {'message': "Hello World!"}
        return Response(content)


class APISignUpView(generics.CreateAPIView):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer


class APILogInView(TokenObtainPairView):
    serializer_class = LogInSerializer
