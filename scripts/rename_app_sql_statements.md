### Rename a django app
* Check out: [Stackoverflow Thread](https://stackoverflow.com/questions/8408046/how-to-change-the-name-of-a-django-app).
* Change any references in views, urls, manage and settings Python files in project.
* Edit DB django_content_type OldAppName -> NewAppName
* Rename the models. <oldAppName>_modelName -> <newAppName>_modelName in DB
* Change Name of Migrations to not run the same again (DB django_migrations). app=oldAppName -> newAppName
### Example renaming "websocket" to "sessionhandler" SQL Statements
``` sql
update django_content_type  set app_label="sessionhandler" where app_label="websocket";
update django_migrations set app_label="sessionhandler" where app_label="websocket";

alter table websocket_databasenotification rename to sessionhandler_databasenotification;
alter table websocket_databaserequestsensor rename to sessionhandler_databaserequestsensor;
alter table websocket_databasesurvey rename to sessionhandler_databasesurvey;
alter table websocket_databasesurveyquestions rename to sessionhandler_databasesurveyquestions;

alter table websocket_microsurvey rename to sessionhandler_microsurvey;
alter table websocket_microsurveyanswers rename to sessionhandler_microsurveyanswers;
alter table websocket_microsurveyanswers_microsurveyquestion rename to sessionhandler_microsurveyanswers_microsurveyquestion;
alter table websocket_microsurveydatabase rename to sessionhandler_microsurveydatabase;
alter table websocket_microsurveyquestion rename to sessionhandler_microsurveyquestion;
alter table websocket_microsurveyquestion_microsurvey rename to sessionhandler_microsurveyquestion_microsurvey;

alter table websocket_notification rename to sessionhandler_notification;
alter table websocket_participant rename to sessionhandler_participant;
alter table websocket_requestsensors rename to sessionhandler_requestsensors;
alter table websocket_session rename to sessionhandler_session;

alter table websocket_study rename to sessionhandler_study;
alter table websocket_study_microsurvey rename to sessionhandler_study_microsurvey;
alter table websocket_study_notifications rename to sessionhandler_study_notifications;
alter table websocket_study_sensor_requests rename to sessionhandler_study_sensor_requests;
alter table websocket_study_surveys rename to sessionhandler_study_surveys;

alter table websocket_survey rename to sessionhandler_survey;
alter table websocket_surveyquestion rename to sessionhandler_surveyquestion;
alter table websocket_surveyquestion_survey rename to sessionhandler_surveyquestion_survey;
```