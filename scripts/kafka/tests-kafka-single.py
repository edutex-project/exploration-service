from sessionhandler import create_connection

SERVER_ADDRESS = "127.0.0.1:8002"

user_data = {
    'username':"user",
    'password':"michael2020"
}

ws = create_connection("ws://"+SERVER_ADDRESS+"/")
print("Sending 'Hello, World'...")
ws.send("Hello, World")
print("Sent")
print("Receiving...")
result =  ws.recv()
print("Received '%s'" % result)
ws.close()