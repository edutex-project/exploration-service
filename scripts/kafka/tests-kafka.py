import json
import requests
from websocket_client.websocket import WebSocketApp

try:
    import thread
except ImportError:
    import _thread as thread

SERVER_ADDRESS = "127.0.0.1:8002"

user_data = {
    'username': "user",
    'password': "michael2020"
}


def authenticate():
    url = "http://" + SERVER_ADDRESS + "/api-token-auth/"
    r = requests.post(url, user_data)
    print("authenticate(): response: " + r.text)
    return json.loads(r.text)["token"]

def on_message(ws, message):
    print("on_message is called")

def on_error(ws, error):
    print("on_error is called " + str(error))

def on_close(ws):
    print("### Websocket is closed ###")

def on_open(ws):
    print("### Websocket is opened ###")
    print("To close sessionhandler type: exit")
    print("Format: <{key:value, key1:value}>")

    def run(*args):

        request = {"client_response": {"name": "PANAS",
                                       "study": "LauraStudy01 Laura      2020-11-11 10:56:53.304402+00:00",
                                       "isSurvey": False, "type": "interval_notification", "schedule": "30",
                                       "question": "Do you feel angry right now?",
                                       "answers": ["Yes", "Don't know", "No"],
                                       "add_answers_allowed": "allow_nothing"}}

        try:
            json_send = json.dumps(request)
            print("Sending json.dumps:" + json_send)
            ws.send(json_send)
        except AssertionError as error:
            print("Error: '" + error + "'.")

    thread.start_new_thread(run, ())

if __name__ == "__main__":

    token = authenticate()
    token_header = "authorization: Token " + str(token)
    participant_header = "participantIdentifier: 0001"
    # sessionhandler.enableTrace(True)
    ws = websocket.WebSocketApp("ws://"+SERVER_ADDRESS+"/ws/socket/loremipsum/",
                                on_open=on_open,
                                on_message=on_message,
                                on_error=on_error,
                                on_close=on_close,
                                header=[token_header, participant_header])
    #
    # ws.run_forever()
