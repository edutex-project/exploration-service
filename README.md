# Exploration Service

The Exploration Service is a Python Django project that orchestrates and collects data for the Edutex infrastructure. Data is send to it from Smartwatches and Smartphones. Furthermore, it is connected to the Interaction Record Store to fuse its interaction data with the sensor information. By usage of a connector it can stream its data to a Kafka data lake.

# Build and Start

```bash
    docker-compose up -d --build
```
 
# Setup

To successfully run the application you first need to setup the database within the docker container.

```bash
    docker-compose exec -it <Container-Name> /bin/bash
    python manage.py makemigrations
    python manage.py migrate

    python manage.py createsuperuser
```

# Authors and acknowledgment
Various developers have contributed since the beginning of this project.

## Core developers
- George-Petru Ciordas-Hertel (since 2020)
- Daniel Biedermann (since 2022)
- Sebastian Rödling (2020 - 2022)

## Previous Student Developers & Researchers
- Nicolas Schulz
- Jakob Vanek
- Lara Tabea Galkowski
- Bianca Lien

# License
This Project is Open Source Software. You can redistribute it and modify it under the terms of the GNU General Public License version 3, as published by the Free Software Foundation, or any later version.

# Project status
This project is currently used in multiple research projects such as Edutex (edutex.de).