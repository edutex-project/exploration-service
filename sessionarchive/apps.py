from django.apps import AppConfig


class SessionarchiveConfig(AppConfig):
    name = 'sessionarchive'
