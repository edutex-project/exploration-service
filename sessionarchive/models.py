from django.db import models
import jsonfield
from sessionhandler.models import Participant


# Create your models here.

class SessionArchive(models.Model):
    participant = models.ForeignKey(Participant, on_delete=models.DO_NOTHING)
    target_address = models.CharField(max_length=256, blank=True, null=True)
    message = jsonfield.JSONField(blank=True, null=True)
    timestamp = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "ID: {0: <10}, Participant: {1:<15}, Part-ID: {2:<20}, Target: {3:<35}".format(str(self.id),
                                                                                             self.participant.name,
                                                                                             self.participant.identifier,
                                                                                             self.target_address)
