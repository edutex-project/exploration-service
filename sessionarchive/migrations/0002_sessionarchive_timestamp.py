# Generated by Django 3.1.2 on 2021-06-02 12:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sessionarchive', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='sessionarchive',
            name='timestamp',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
