from datetime import datetime

from django.db.models import Q, F
from django.shortcuts import render

# Create your views here.
from rest_framework.decorators import api_view
from django.http.response import JsonResponse
from sessionhandler.models import Study
from restframework.serializers import StudySerializer


@api_view(['GET'])
def studies_list_published(request):
    current_date = datetime.now().date()
    active_studies = Study.objects.filter(Q(expiration_date__gte=current_date), Q(starting_date__lte=current_date))

    if request.method == 'GET':
        study_serializer = StudySerializer(active_studies, many=True)
        return JsonResponse(study_serializer.data, safe=False)


