from django.conf.urls import url
from restframework import views

urlpatterns = [
    url(r'api/studies$', views.studies_list_published),
]