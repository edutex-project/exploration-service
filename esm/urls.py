# For URL and Routing
from django.contrib import admin
from django.urls import path, include
import signal
# For channels_presence
from channels_presence.models import Room, Presence
# For authentification
from django.contrib.auth import views as auth_views
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets

app_name = "esm"


urlpatterns = [
    path('acra/', include("acra.urls")),
    path('admin/', admin.site.urls, name='admin_page'),
    path('socket/', include('sessionhandler.urls')),
    path('', include('core.urls')),
    path('', include('authentification.urls')),
    path('', include('restframework.urls'))
]

# This code runs one time if you start the Server by command python3 manage.py runserver
from sessionhandler.scheduler import start
start()
