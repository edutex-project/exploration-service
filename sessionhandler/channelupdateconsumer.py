from channels.generic.websocket import WebsocketConsumer
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from channels_presence.signals import presence_changed
from django.dispatch import receiver
from channels_presence.models import Presence
import json


class ChannelUpdateConsumer(WebsocketConsumer):
    """ This Class supports the index.html observation of active sessionhandler connections """

    def connect(self):
        self.room_name = self.scope['url_route']['kwargs']
        self.room_group_name = 'default'

        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )
        self.accept()

    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    def receive(self, text_data):
        pass

    # Receive message from room group
    def chat_message(self, event):
        pass

    def forward_message(self, event):
        self.send(event["message"])


channel_layer = get_channel_layer()


@receiver(presence_changed)
def broadcast_presence(sender, room, **kwargs):
    curr_presence = Presence.objects.all()
    presence = list(set([i.room.channel_name.split("_")[1] for i in curr_presence]))

    message = {
        "type": "presence",
        "payload": {
            "channel_name": room.channel_name,
            "lurkers": room.get_anonymous_count(),
        },
        "active_rooms": {
            "online": presence
        }
    }

    channel_layer_message = {
        "type": "forward.message",
        "message": json.dumps(message)
    }

    async_to_sync(channel_layer.group_send)('default', channel_layer_message)
