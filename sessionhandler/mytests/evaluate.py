from sessionarchive.models import *
import json
from sessionhandler.models import *


def print_pretty(tag, timestamp, name):
    print(tag, name, timestamp)


def evaluate_should(running_time, study):
    eva = {
        "Pre Survey": [1],
        "Post Survey": [1],
        "Single Survey": [0],
        "Interval Survey": [0],
        "Single Notification": [0],
        "Interval Notification": [0],
        "Microsurveys": [0],
        "Interval SensorRequest": [0],
        "Single SensorRequest": [0],
    }


    surveys = Survey.objects.filter(study=study)
    for surv in surveys:
        if surv.type == "single_survey":
            if int(surv.offset) <= running_time:
                eva["Single Survey"] = [eva["Single Survey"][0] + 1]

        if surv.type == "interval_survey":
            if int(surv.offset) <= running_time:
                adds = 1
                rest_time = running_time - int(surv.offset)
                while True:
                    if rest_time - int(surv.schedule) > 0:
                        rest_time -= int(surv.schedule)
                        adds += 1
                    else:
                        break
                eva["Interval Survey"] = [eva["Interval Survey"][0] + adds]

    notifications = Notification.objects.filter(study=study)
    for elem in notifications:
        if elem.type == "single_notification":
            if int(elem.offset) <= running_time:
                eva["Single Notification"] = [eva["Single Notification"][0] + 1]

        if elem.type == "interval_notification":
            if int(elem.offset) <= running_time:
                adds = 1
                rest_time = running_time - int(elem.offset)
                while True:
                    if rest_time - int(elem.schedule) > 0:
                        rest_time -= int(elem.schedule)
                        adds += 1
                    else:
                        break
                eva["Interval Notification"] = [eva["Interval Notification"][0] + adds]

    microsurveys = MicroSurvey.objects.filter(study=study)
    for elem in microsurveys:
        if elem.type == "single_microsurvey":
            if int(elem.offset) <= running_time:
                eva["Microsurveys"] = [eva["Microsurveys"][0] + 1]

        if elem.type == "interval_microsurvey":
            if int(elem.offset) <= running_time:
                adds = 1
                rest_time = running_time - int(elem.offset)
                while True:
                    if rest_time - int(elem.schedule) > 0:
                        rest_time -= int(elem.schedule)
                        adds += 1
                    else:
                        break
                eva["Microsurveys"] = [eva["Microsurveys"][0] + adds]

    sensorrequests = RequestSensors.objects.filter(study=study)
    for elem in sensorrequests:
        if elem.type == "single_request":
            if int(elem.offset) <= running_time:
                eva["Single SensorRequest"] = [eva["Single SensorRequest"][0] + 1]

        if elem.type == "interval_request":
            if int(elem.offset) <= running_time:
                adds = 1
                rest_time = running_time - int(elem.offset)
                while True:
                    if rest_time - int(elem.schedule) > 0:
                        rest_time -= int(elem.schedule)
                        adds += 1
                    else:
                        break
                eva["Interval SensorRequest"] = [eva["Interval SensorRequest"][0] + adds]

    return eva

def evaluate(runningtime, study):
    should = evaluate_should(runningtime, study)

    messages = SessionArchive.objects.filter(target_address="socket_loremipsum")

    msg_json = [(i.message, i.timestamp) for i in messages]

    survey_single = []
    survey_interval = []
    notification_single = []
    notification_interval = []
    sensorrequest_single = []
    sensorrequest_intveral = []
    microsurvey_single = []
    microsurvey_interval = []

    for js in msg_json:
        msg = dict(js[0])
        timestamp = str(js[1])

        if msg["type"] == "pre_survey":
            if "fake" in msg:
                print("Pre Survey: " + "Fake" + " at: " + timestamp)
            else:
                print("Pre Survey: " + msg["name"] + " at: " + timestamp)

        elif msg["type"] == "single_microsurvey":
            microsurvey_single.append((timestamp, msg["name"]))

        elif msg["type"] == "interval_microsurvey":
            microsurvey_interval.append((timestamp, msg["name"]))

        elif msg["type"] == "microSurvey":
            microsurvey_interval.append((timestamp, msg["name"]))

        elif msg["type"] == "single_notification":
            notification_single.append((timestamp, msg["name"]))
        elif msg["type"] == "interval_notification":
            notification_interval.append((timestamp, msg["name"]))

        elif msg["type"] == "single_survey":
            survey_single.append((timestamp, msg["name"]))
        elif msg["type"] == "interval_survey":
            survey_interval.append((timestamp, msg["name"]))

        elif msg["type"] == "single_request":
            sensorrequest_single.append((timestamp, msg["name"]))

        elif msg["type"] == "interval_request":
            sensorrequest_intveral.append((timestamp, msg["name"]))

        elif msg["type"] == "post_survey":
            if "fake" in msg:
                print("Post Survey: " + "Fake" + " at: " + timestamp)
            else:
                print("Post Survey: " + msg["name"] + " at: " + timestamp)

    should["Pre Survey"].append(1)
    should["Post Survey"].append(1)
    should["Single Survey"].append(len(survey_single))
    should["Interval Survey"].append(len(survey_interval))
    should["Single Notification"].append(len(notification_single))
    should["Interval Notification"].append(len(notification_interval))
    should["Microsurveys"].append(len(microsurvey_interval))
    should["Single SensorRequest"].append(len(sensorrequest_single))
    should["Interval SensorRequest"].append(len(sensorrequest_intveral))

    for key in should.keys():
        print(key, should[key])