import json
import requests
import websocket
import time
import threading

try:
    import thread
except ImportError:
    import _thread as thread

SERVER_ADDRESS = "127.0.0.1:8000"

user_data = {
    'username': "testuser",
    'password': "test"
}


def inform_received(message):
    print("Received: " + message)
    # print()


def inform_send(message):
    print("Sended: " + message)
    # print()


def authenticate():
    url = "http://" + SERVER_ADDRESS + "/api-token-auth/"
    r = requests.post(url, user_data)
    print("authenticate(): response: " + r.text)
    return json.loads(r.text)["token"]


def on_message(ws, message):
    message = json.loads(message)

    if "participant_finished" in message:
        inform_received("Participant finished" + str(message["participant_finished"]))
        ws.send(json.dumps({"action_pre_session_surveys": 1}))
        inform_send("Request Pre Survey")

    elif "type" in message:

        inform_received(message["name"])

        if message["type"] == "pre_survey":
            ws.send(json.dumps({"action_start_session": 1}))
            inform_send("Request Start Session")

        if "fake" not in message:
            message["androidID"] = "TestWebsocket"
            message["userID"] = "TestWebsocket"
            message["user_answer"] = "DebugSocket"
            ws.send(json.dumps({"client_response": message}))

    elif "isRecovery" in message:

        if message["isRecovery"] == True:
            ws.send(json.dumps({"action_force_stop": 1}))
            ws.send(json.dumps({"action_pre_session_surveys": 1}))
            inform_send("Request Pre Survey after Force Stop")


def on_error(ws, error):
    pass


def on_close(ws):
    print("### Websocket is closed ###")


def on_open(ws):
    print("### Websocket is opened ###")

    def run(*args):

        request = {"action_recover_last_session_if_available": 1}

        try:
            json_send = json.dumps(request)
            inform_send("Recover last Session if available.")
            ws.send(json_send)
        except AssertionError as error:
            print("Error: '" + error + "'.")

    thread.start_new_thread(run, ())


def call_close(ws, seconds):
    print("Thread closing is online..")
    time.sleep(seconds)
    ws.send(json.dumps({"action_post_session": 1}))
    inform_send("Stopping Thread: " + "Requested Post Survey")
    time.sleep(5)
    ws.close()


def run_websocket(seconds):
    token = authenticate()
    token_header = "authorization: Token " + str(token)
    participant_header = "participantIdentifier: 390e1db9-4c45-451a-8e5c-fcc6a8925188"
    # websocket.enableTrace(True)
    ws = websocket.WebSocketApp("ws://" + SERVER_ADDRESS + "/ws/socket/loremipsum/",
                                on_open=on_open,
                                on_message=on_message,
                                on_error=on_error,
                                on_close=on_close,
                                header=[token_header, participant_header])

    stop_thread = threading.Thread(target=call_close, args=(ws, seconds,))
    stop_thread.start()

    ws.run_forever()
