from tablib import Dataset
from django.contrib.auth.models import User
from sessionhandler.models import *
from import_export import resources
import json
import logging

USERNAME = 'testuser'
USERNAME_PW = 'test'

logger = logging.getLogger(__name__)
USE_LOGGER = False

INPUTS = [("Test_SensorRequest.json", RequestSensors),

          ("Test_MicroSurvey.json", MicroSurvey),
          ("Test_MicroSurvey_Questions.json", MicroSurveyQuestion),
          ("Test_MicroSurvey_Answers.json", MicroSurveyAnswers),

          ("Test_Notifications.json", Notification),


          ("Test_Survey.json", Survey),
          ("Test_SurveyQuestions.json", SurveyQuestion),

          ("Test_Studies.json", Study),
          ("Test_Participant.json", Participant)]


def inform(text: str):
    if USE_LOGGER:
        logger.info(text)
    else:
        print(text)


def invert_list_str(string):
    tmp = string.split(",")
    if tmp == [""]:
        return ""
    new = []
    for i in tmp:
        try:
            curr = int(i)
            if curr > 0:
                curr = -curr

            new.append(str(curr))
        except:
            print("Error: " + i)

    return ",".join(new)


def import_json(file, model):
    with open("sessionhandler/mytests/" + file, "r") as fh:
        imported_data = Dataset().load(fh)

        attention = ["id",
                     "sensorrequest",
                     "display_order_sequence",
                     "microsurvey",
                     "study",
                     "user",
                     "microsurveyquestion",
                     "notifications",
                     "surveys",
                     "survey",
                     "sensor_requests"]

        tmp_header = imported_data.headers
        for att in attention:
            if att in tmp_header:
                indx = tmp_header.index(att)
                counter = len(imported_data)
                for i in range(counter):
                    row = list(imported_data.lpop())
                    current_value = row[indx]
                    if type(current_value) == type("string"):
                        new_value = invert_list_str(current_value)
                        row[indx] = new_value
                    else:
                        row[indx] = -row[indx] if row[indx] > 0 else row[indx]
                    imported_data.rpush(tuple(row))

        resource = resources.modelresource_factory(model=model)()
        result = resource.import_data(imported_data, dry_run=True)

        if not result.has_errors():
            resource.import_data(imported_data, dry_run=False)
            print("Imported " + file)
        if result.has_errors():
            print("Has Error")


def import_debug_entries():
    for elem in INPUTS:
        import_json(elem[0], elem[1])


def create_user():
    query_users = User.objects.filter(username=USERNAME)
    if not query_users:

        User.objects.create_user(username=USERNAME,
                                 email='testing@test.com',
                                 password=USERNAME_PW,
                                 id=-1)

        inform("Testuser created.")
    else:
        inform("Testuser already registered.")
