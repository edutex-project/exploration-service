from django.urls import path
from .views import create_room, room

app_name = "sessionhandler"

urlpatterns = [
    path('', create_room, name='create_room'),
    path('<str:room_name>/', room, name='view_room')
]

# This Code is only will only run by Start Up
