# Generated by Django 3.1.2 on 2020-11-13 09:49

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sessionhandler', '0007_databaserequestsensor_timestamp_asked'),
    ]

    operations = [
        migrations.RenameField(
            model_name='databaserequestsensor',
            old_name='timestamp_asked',
            new_name='timestamp_requested',
        ),
    ]
