# Generated by Django 3.1.2 on 2021-04-06 10:21

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sessionhandler', '0053_participant_restricuted_usage'),
    ]

    operations = [
        migrations.RenameField(
            model_name='participant',
            old_name='restricuted_usage',
            new_name='restricted_usage',
        ),
    ]
