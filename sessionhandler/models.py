import jsonfield
from django.db import models
from django import forms
import json
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

ANSWER_EDIT_TYPES = [
    ("allow_voice", "Allow SpeechToText"),
    ("allow_keyboard", "Allow Keyboard answers"),
    ("allow_both", "Allow Keyboard and SpeechToText"),
    ("allow_nothing", "User is not allowed to generate Input")
]

REQUEST_SENSOR_BOOL = [
    ("request", "Request the Sensor"),
    ("do_not_request", "Dont request the Sensor"),
]

REQUEST_SENSOR_DELAY = [
    ("0", "[0ms] SENSOR_DELAY_FASTEST"),
    ("1", "[20ms] SENSOR_DELAY_GAME"),
    ("3", "[200ms] SENSOR_DELAY_NORMAL"),
    ("2", "[60ms] SENSOR_DELAY_UI"),
]


class RequestSensors(models.Model):
    name = models.CharField(max_length=256)

    # study = models.ForeignKey(to=Study, on_delete=models.CASCADE, blank=True, null=True)

    type = models.CharField(max_length=32,
                            choices=[
                                # ("pre_request", "Pre-session"),
                                ("single_request", "Fixed Time"),
                                ("interval_request", "Interval"),
                                # ("post_request", "Post-session"),
                            ],
                            default="single_request",
                            verbose_name="Schedule Type",
                            help_text="Requesting Sensor for Schedule")

    schedule = models.CharField(max_length=56,
                                choices=[(str(i), i) for i in range(0, 10000)],
                                default="0",
                                verbose_name="Interval Time",
                                help_text="in minutes")

    offset = models.IntegerField(help_text="",
                                 default=0)

    start_immediately = models.BooleanField(default=True, help_text="Boolean, if first run is at time 0.")

    # Sensors
    ## Captured by Phone
    non_physical_environment_data = models.CharField(max_length=56,
                                                     choices=REQUEST_SENSOR_BOOL,
                                                     default="do_not_request",
                                                     null=True,
                                                     blank=True,
                                                     help_text="Sensors: Bluetooth, WiFi, GPS")
    gps = models.CharField(max_length=56, choices=REQUEST_SENSOR_BOOL, default="do_not_request", null=True, blank=True)
    bluetooth = models.CharField(max_length=56, choices=REQUEST_SENSOR_BOOL, default="do_not_request", null=True,
                                 blank=True)

    ## Captured by Smartwatch
    ### Physical Envorironment Data

    physical_environment_data = models.CharField(max_length=56,
                                                 choices=REQUEST_SENSOR_BOOL,
                                                 default="do_not_request",
                                                 null=True,
                                                 blank=True,
                                                 help_text="Sensors: Ambientlight, Ambientnoise, Temperature")

    recording_time_physical_environment_data = models.CharField(max_length=56,
                                                                choices=[(str(i), i) for i in range(30, 605, 5)],
                                                                default="30",
                                                                null=True, blank=True, help_text="in seconds")
    fidelity_physical_environment_data = models.CharField(max_length=56, choices=REQUEST_SENSOR_DELAY, default="3",
                                                          null=True, blank=True)

    microphone = models.CharField(max_length=56, choices=REQUEST_SENSOR_BOOL, default="do_not_request",
                                  null=True, blank=True)
    recording_time_microphone = models.CharField(max_length=56, choices=[(str(i), i) for i in range(5, 605, 5)],
                                                 default="5",
                                                 null=True, blank=True, help_text="in seconds")

    temperature = models.CharField(max_length=56, choices=REQUEST_SENSOR_BOOL, default="do_not_request", null=True,
                                   blank=True)
    recording_time_temperature = models.CharField(max_length=56, choices=[(str(i), i) for i in range(5, 605, 5)],
                                                  default="5",
                                                  null=True, blank=True, help_text="in seconds")
    fidelity_temperature = models.CharField(max_length=56, choices=REQUEST_SENSOR_DELAY, default="3",
                                            null=True, blank=True)

    ### Physiological Data
    physiological_data = models.CharField(max_length=56,
                                          choices=REQUEST_SENSOR_BOOL,
                                          default="do_not_request",
                                          null=True,
                                          blank=True,
                                          help_text="Sensors: Heartrate, Heartbeat, SDNN, Oxygen Saturation")
    recording_time_physiological_data = models.CharField(max_length=56, choices=[(str(i), i) for i in range(60, 605, 5)],
                                                         default="60",
                                                         null=True, blank=True, help_text="in seconds")
    fidelity_physiological_data = models.CharField(max_length=56, choices=REQUEST_SENSOR_DELAY, default="3",
                                                   null=True, blank=True)

    heartbeat = models.CharField(max_length=56, choices=REQUEST_SENSOR_BOOL, default="do_not_request", null=True,
                                 blank=True)
    recording_time_heartbeat = models.CharField(max_length=56, choices=[(str(i), i) for i in range(15, 605, 5)],
                                                default="15",
                                                null=True, blank=True, help_text="in seconds")
    fidelity_heartbeat = models.CharField(max_length=56, choices=REQUEST_SENSOR_DELAY, default="3",
                                          null=True, blank=True)

    oxygen_saturation = models.CharField(max_length=56, choices=REQUEST_SENSOR_BOOL, default="do_not_request",
                                         null=True,
                                         blank=True)
    recording_time_oxygen_saturation = models.CharField(max_length=56, choices=[(str(i), i) for i in range(15, 605, 5)],
                                                        default="15",
                                                        null=True, blank=True, help_text="in seconds")
    fidelity_oxygen_saturation = models.CharField(max_length=56, choices=REQUEST_SENSOR_DELAY, default="3",
                                                  null=True, blank=True)

    heartrate_variability = models.CharField(max_length=56, choices=REQUEST_SENSOR_BOOL, default="do_not_request",
                                             null=True,
                                             blank=True)
    recording_time_heartrate_variability = models.CharField(max_length=56,
                                                            choices=[(str(i), i) for i in range(15, 605, 5)],
                                                            default="15",
                                                            null=True, blank=True, help_text="in seconds")
    fidelity_heartrate_variability = models.CharField(max_length=56, choices=REQUEST_SENSOR_DELAY, default="3",
                                                      null=True, blank=True)

    ### Behavioral Data

    behavioral_data = models.CharField(max_length=56,
                                       choices=REQUEST_SENSOR_BOOL,
                                       default="do_not_request",
                                       null=True,
                                       blank=True,
                                       help_text="Sensors: Accelerometer")
    recording_time_behavioral_data = models.CharField(max_length=56, choices=[(str(i), i) for i in range(5, 605, 5)],
                                                      default="5",
                                                      null=True, blank=True, help_text="in seconds")
    fidelity_behavioral_data = models.CharField(max_length=56, choices=REQUEST_SENSOR_DELAY, default="3",
                                                null=True, blank=True)

    def __str__(self):
        return "Request: {0: <15} ({2: <6}, schedule= {1: <5})".format(str(self.name), str(self.schedule),
                                                                       str(self.type))



    class Meta:
        verbose_name_plural = "07. Sensor Requests"




## Models for ESM SURVEYS & NOTIFICATIONmicrosurveyS

# NOTIFICATIONS
class Notification(models.Model):
    # study = models.ForeignKey(to=Study, null=True, blank=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=256)
    type = models.CharField(max_length=128,
                            choices=[
                                ("single_notification", "Fixed Time"),
                                ("interval_notification", "Interval"),
                            ],
                            default="single_notification",
                            verbose_name="Schedule Type",
                            help_text="Single Surveys werden einmalig nach einer Zeit X ausgeführt. Interval Surveys werde alle X Minuten "
                                      "ausgeführt")
    schedule = models.CharField(max_length=5, choices=[
                                                          (str(i), i) for i in range(0, 9999)
                                                      ] + [("9999", 9999)], default="0", help_text="in minutes",
                                verbose_name="Interval Time",
                                )

    offset = models.IntegerField(help_text="If type == interval, you can use the offset "
                                           "to specify a new starting point from which "
                                           "the interval should be used. (Positiv Integer representing min.)",
                                 default=0)

    start_immediately = models.BooleanField(default=True, help_text="Boolean, if first run is at time 0.")

    question = models.CharField(max_length=256, help_text="zB: Wie geht es"
                                                          " dir?")
    text_answer = models.CharField(max_length=256, blank=True, null=True,
                                   help_text="zB: 'Ja, Vielleicht, Nein' oder leer lassen falls per "
                                             "Voice geantwortet werden soll", )
    numerical_answer_lower = models.IntegerField(help_text="Lower Bound", blank=True, null=True)
    numerical_answer_upper = models.IntegerField(help_text="Upper Bound", blank=True, null=True)
    add_answers_allowed = models.CharField(max_length=30, choices=ANSWER_EDIT_TYPES, default="allow"
                                                                                             "_nothing",
                                           help_text="Is the user allowed to generate o"
                                                     "wn Input")

    sensorrequest = models.ManyToManyField(RequestSensors, blank=True)

    def __str__(self):
        return "Question: {2: <15} ({0: <6}, schedule= {1: <5})".format(self.name, self.schedule, self.question)

    def clean(self):
        if (self.text_answer is None or self.text_answer == "") and \
                self.numerical_answer_upper is None and self.numerical_answer_lower is None:

            raise ValidationError({'text_answer': 'Even one of text_answer or numerical_answer_lower and '
                                                  'numerical_answer_upper should have a value.'})

        elif (not (self.text_answer is None or self.text_answer == "")) and not (
                self.numerical_answer_upper is None) and self.numerical_answer_lower is None:
            raise ValidationError({'text_answer': 'Even one of text_answer or numerical_answer_lower and '
                                                  'numerical_answer_upper should have a value.'})

        elif (not (self.text_answer is None or self.text_answer == "")) and not (
                self.numerical_answer_lower is None) and self.numerical_answer_upper is None:
            raise ValidationError({'text_answer': 'Even one of text_answer or numerical_answer_lower and '
                                                  'numerical_answer_upper should have a value.'})
        elif (not (self.text_answer is None or self.text_answer == "")) and not (
                self.numerical_answer_lower is None) and not (self.numerical_answer_upper is None):
            raise ValidationError({'text_answer': 'Even one of text_answer or numerical_answer_lower and '
                                                  'numerical_answer_upper should have a value.'})

    class Meta:
        verbose_name_plural = "06. Notifications"


class DatabaseNotification(models.Model):
    name = models.CharField(max_length=256, blank=True, null=True)
    study = models.CharField(max_length=256, blank=True, null=True)
    type = models.CharField(max_length=256, blank=True, null=True)
    schedule = models.CharField(max_length=256, blank=True, null=True)
    sessionID = models.CharField(max_length=256, blank=True, null=True)
    androidID = models.CharField(max_length=256, blank=True, null=True)
    userID = models.CharField(max_length=256, blank=True, null=True)
    timestamp_question_asked = jsonfield.JSONField()
    timestamp_question_answered = models.CharField(max_length=256, blank=True, null=True)
    identifier = models.CharField(max_length=256, blank=True, null=True)
    question = models.CharField(max_length=256, blank=True, null=True)
    answers = models.CharField(max_length=256, blank=True, null=True)
    add_answers_allowed = models.CharField(max_length=256, blank=True, null=True)
    user_answer = models.CharField(max_length=256, blank=True, null=True)
    notification_id = models.CharField(max_length=255, blank=True, null=True)
    event_id = models.CharField(max_length=255, blank=True, null=True)

    def last_X_entries(self, amount):
        return DatabaseNotification.order_by('-timestamp').all()[:amount]

    def __str__(self):
        return "Notification {0: <25} ({1: <15}, {2: <30})".format(self.name, self.type,
                                                                   self.timestamp_question_answered)

    class Meta:
        verbose_name_plural = "12. DB Notifications"


# SURVEYS

class Survey(models.Model):
    name = models.CharField(max_length=255, unique=True)
    type = models.CharField(max_length=15,
                            choices=[
                                ("pre_survey", "Pre-session"),
                                ("single_survey", "Fixed time"),
                                ("interval_survey", "Interval"),
                                ("post_survey", "Post-session"),
                            ],
                            default="single_survey",
                            verbose_name="Schedule Type",
                            help_text="")

    offset = models.IntegerField(help_text="The time in minutes relative to the start of the session when"
                                           " the whole questionnaire is displayed",
                                 default=0)

    schedule = models.CharField(max_length=5,
                                choices=[(str(i), i) for i in range(0, 10000)],
                                default="0",
                                help_text='The time in minutes when the questionnaire is displayed again.',
                                verbose_name="Interval Time")

    start_immediately = models.BooleanField(default=True,
                                            help_text="Boolean, if first run is at time 0.")

    display_order_sequence = models.CharField(max_length=255,
                                              blank=True,
                                              help_text="Separate SurveyQuestion ID's by "
                                                        "comma. No sequence means no "
                                                        "guaranteed ordering.")

    sensorrequest = models.ManyToManyField(RequestSensors, blank=True)

    def __str__(self):
        return "Questionnaire: {0: <15} ({2: <6}, schedule= {1: <5})".format(self.name, self.schedule, self.type)

    class Meta:
        verbose_name_plural = "05. Questionnaire"


class SurveyQuestion(models.Model):
    # survey = models.ForeignKey(Survey, on_delete=models.CASCADE)

    display_order = models.CharField(max_length=128, choices=[(str(i), i) for i in range(1, 16)], default="1")
    display_type = models.CharField(max_length=10, choices=[
        ("Slider", "Display Slider"),
        ("Choices", "Display Single Choices"),
        # ("Checkbox", "Display Multiple Choices"),
    ], default="Choices")
    question = models.CharField(max_length=70, help_text="zB: Wie geht es dir?")
    text_answer = models.CharField(max_length=256, blank=True, null=True,
                                   help_text="zB: 'Ja, Vielleicht, Nein' oder leer lassen falls"
                                             " per Voice geantwortet werden soll")
    numerical_answer_lower = models.IntegerField(help_text="Lower Bound", blank=True, null=True)
    numerical_answer_upper = models.IntegerField(help_text="Upper Bound", blank=True, null=True)
    add_answers_allowed = models.CharField(max_length=50, choices=ANSWER_EDIT_TYPES, default="allow_"
                                                                                             "nothing",
                                           help_text="Funktioniert nur bei Display "
                                                     "Type: Choices")
    survey = models.ManyToManyField(Survey, blank=True)

    def __str__(self):

        if self.text_answer != None:
            return "ID: {0}, Question: {1: <15}, Answers: {2}, Display as: {3}".format(self.id, self.question,
                                                                                       self.text_answer,
                                                                                       self.display_type)
        else:
            return "ID: {0}, Question: {1: <15}, Answers: {2} - {3}, Display as: {4}".format(self.id,
                                                                                             self.question,
                                                                                             self.numerical_answer_lower,
                                                                                             self.numerical_answer_upper,
                                                                                             self.display_type)

    class Meta:
        verbose_name_plural = "10. Questionnaire Q's"


class DatabaseSurvey(models.Model):
    name = models.CharField(max_length=256, blank=True, null=True)
    study = models.CharField(max_length=256, blank=True, null=True)
    type = models.CharField(max_length=256, blank=True, null=True)
    schedule = models.IntegerField()
    sessionID = models.CharField(max_length=256, blank=True, null=True)
    androidID = models.CharField(max_length=256, blank=True, null=True)
    userID = models.CharField(max_length=256, blank=True, null=True)
    timestamp_question_asked = jsonfield.JSONField()
    timestamp_question_answered = models.CharField(max_length=256, blank=True, null=True)
    identifier = models.CharField(max_length=256, blank=True, null=True)
    survey_id = models.CharField(max_length=255, blank=True, null=True)
    event_id = models.CharField(max_length=255, blank=True, null=True)

    def last_X_entries(self, amount):
        return DatabaseSurvey.order_by('-timestamp').all()[:amount]

    def __str__(self):
        return "Name: {0: <20},SessionID: {1: <50}, Answered {2: <30}, Type: {3: <30}".format(self.name, self.sessionID,
                                                                                              self.timestamp_question_answered,
                                                                                              self.type)

    class Meta:
        verbose_name_plural = "14. DB Questionnaires"


class DatabaseSurveyQuestions(models.Model):
    survey = models.ForeignKey(DatabaseSurvey, on_delete=models.CASCADE)
    display_type = models.CharField(max_length=256, blank=True, null=True)
    display_order = models.CharField(max_length=256, blank=True, null=True)
    question = models.CharField(max_length=256, blank=True, null=True)
    answers = models.CharField(max_length=512, blank=True, null=True)
    add_answers_allowed = models.CharField(max_length=256, blank=True, null=True)
    user_answer = models.CharField(max_length=256, blank=True, null=True)
    survey_question_id = models.CharField(max_length=255, blank=True, null=True)

    def last_X_entries(self, amount):
        return DatabaseSurveyQuestions.order_by('-timestamp').all()[:amount]

    def __str__(self):
        answer = ""
        possible_answers = self.answers[1:-1].split(",")
        if (self.add_answers_allowed == "allow_nothing"):
            try:
                answer = possible_answers[int(self.user_answer)]
            except:
                answer = self.user_answer
        else:
            try:
                answer = possible_answers[int(self.user_answer)]
            except:
                answer = self.user_answer

        return "Question Nr. {0: <5}, Question: {1: <30}, Answer: {2: <30}".format(self.display_order, self.question,
                                                                                   answer)

    class Meta:
        verbose_name_plural = "11. DB Questions"


# Micro Surveys

class MicroSurvey(models.Model):
    name = models.CharField(max_length=255, unique=True)
    type = models.CharField(max_length=32,
                            choices=[
                                ("single_microsurvey", "Fixed Time"),
                                ("interval_microsurvey", "Interval"),
                            ],
                            default="single_microsurvey",
                            verbose_name="Schedule Type")

    randomize = models.CharField(max_length=5,
                                 choices=[("True", "Yes"), ("False", "No")],
                                 default="No",
                                 verbose_name="Randomize Questions Sequence")

    display_order_sequence = models.CharField(max_length=255,
                                              blank=True,
                                              help_text="Separate Question ID's by comma. No sequence means no "
                                                        "guaranteed ordering.")

    schedule = models.CharField(max_length=5,
                                choices=[(str(x), str(x)) for x in range(0, 9999)],
                                default="20",
                                help_text="The time in minutes relative to the start of the session when an interval "
                                          "is starting Interval",
                                verbose_name="Questionnaire Time Period")

    offset = models.IntegerField(help_text="Starting point (min) of micro Questionnaire",
                                 default=0)

    start_immediately = models.BooleanField(default=True, help_text="Boolean, if first run is at time 0.")

    questions_schedule = models.CharField(choices=[("equidistance", "Equidistance"), ("random", "Randomly")],
                                          default="equidistance",
                                          max_length=64, )

    timeBuffer = models.CharField(max_length=5, choices=[(str(x), str(x)) for x in range(0, 9999)], default="0",
                                  help_text="Buffering Time, Production: Minutes, Debug: Seconds")

    timeBufferDirection = models.CharField(max_length=16, choices=[("both", "add xor subtract"),
                                                                   ("negative", "substract"),
                                                                   ("positiv", "add")], default="both")

    questionLifetime = models.CharField(choices=[("do_remove", "Force remove question"),
                                                 ("do_not_remove", "Do not force remove question")],
                                        default="do_not_remove",
                                        max_length=64,
                                        verbose_name="Lifetime of a question",
                                        help_text="Remove question after 1 Min.")

    remindOnceAfter = models.CharField(choices=[("do_remind", "Remind once"),
                                                ("do_not_remind", "Don't remind")],
                                       default="do_not_remind",
                                       max_length=64,
                                       verbose_name="Reminder Notification",
                                       help_text="Renotify if question was not answered after 1 Min.")

    sensorrequest = models.ManyToManyField(RequestSensors, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "04. Micro Questionnaires"

    def clean(self):
        if self.randomize == "True" and (self.display_order_sequence != "" or self.display_order_sequence is None):
            raise ValidationError({'randomize': 'If you choose randomize please remove the sequence.'})

        if self.display_order_sequence != "":
            try:
                sequence = [int(i.strip()) for i in self.display_order_sequence.split(",")]
                set_sequence = set(sequence)
                if len(set_sequence) != len(sequence):
                    raise ValidationError({'display_order_sequence': ''})
            except:
                raise ValidationError({'display_order_sequence': 'Non int values used. Or values are doubled.'})





class MicroSurveyQuestion(models.Model):
    question = models.CharField(max_length=70, help_text="Max length is set to 70 characters.", blank=True, null=True)
    microsurvey = models.ManyToManyField(MicroSurvey, blank=True)

    def __str__(self):
        return "DB ID:" + str(self.id) + " Q: " + str(self.question)

    class Meta:
        verbose_name_plural = "08. Micro Questionnaire Q's"


class MicroSurveyAnswers(models.Model):
    first_answer = models.CharField(max_length=20, blank=True, null=True)
    second_answer = models.CharField(max_length=20, blank=True, null=True)
    third_answer = models.CharField(max_length=20, blank=True, null=True)
    microsurveyquestion = models.ManyToManyField(MicroSurveyQuestion, blank=True)

    def __str__(self):
        return "{0:<20}, {1:<20}, {2:<20}".format(str(self.first_answer), str(self.second_answer),
                                                  str(self.third_answer))

    class Meta:
        verbose_name_plural = "09. Micro Questionnaire A's"


class MicroSurveyDatabase(models.Model):
    name = models.CharField(max_length=256, blank=True, null=True)
    study = models.CharField(max_length=256, blank=True, null=True)
    type = models.CharField(max_length=256, blank=True, null=True)
    schedule = models.CharField(max_length=256, blank=True, null=True)
    sessionID = models.CharField(max_length=256, blank=True, null=True)
    androidID = models.CharField(max_length=256, blank=True, null=True)
    userID = models.CharField(max_length=256, blank=True, null=True)
    timestamp_question_asked = jsonfield.JSONField()
    timestamp_question_answered = models.CharField(max_length=256, blank=True, null=True)
    identifier = models.CharField(max_length=256, blank=True, null=True)
    question = models.CharField(max_length=256, blank=True, null=True)
    answers = models.CharField(max_length=256, blank=True, null=True)
    user_answer = models.CharField(max_length=256, blank=True, null=True)
    microsurvey_id = models.CharField(max_length=255, blank=True, null=True)
    microsurvey_question_id = models.CharField(max_length=255, blank=True, null=True)
    microsurvey_answer_id = models.CharField(max_length=255, blank=True, null=True)
    event_id = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        verbose_name_plural = "11. DB Micro Questionnaires"

    def __str__(self):
        return "{0:<20}, {1:<20}, {2:<20}".format(str(self.name), str(self.question),
                                                  str(self.user_answer))


# OTHERS


class DatabaseRequestSensor(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    userID = models.CharField(max_length=255, blank=True, null=True)
    sessionID = models.CharField(max_length=255, blank=True, null=True)
    study = models.CharField(max_length=255, blank=True, null=True)
    type = models.CharField(max_length=255, blank=True, null=True)
    schedule = models.CharField(max_length=255, blank=True, null=True)
    event_id = models.CharField(max_length=255, blank=True, null=True)

    # Sensors
    microphone = models.CharField(max_length=255, blank=True, null=True)
    wifi = models.CharField(max_length=255, blank=True, null=True)
    ambientlight = models.CharField(max_length=255, blank=True, null=True)
    bluetooth = models.CharField(max_length=255, blank=True, null=True)
    gps = models.CharField(max_length=255, blank=True, null=True)
    heartrate = models.CharField(max_length=255, blank=True, null=True)
    heartbeat = models.CharField(max_length=255, blank=True, null=True)
    accelerometer = models.CharField(max_length=255, blank=True, null=True)
    temperature = models.CharField(max_length=255, blank=True, null=True)

    timestamp_requested = models.CharField(max_length=255, blank=True, null=True)
    sensor = models.CharField(max_length=255, blank=True, null=True)
    timestamps = jsonfield.JSONField(blank=True, null=True)
    values = jsonfield.JSONField(blank=True, null=True)
    sensorrequest_id = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return "Name: {0: <10}, User ID: {1: <15}, Sensor: {2: <15}".format(self.name, self.userID, self.sensor)

    class Meta:
        verbose_name_plural = "13. DB RequestSensors"


# GENERAL
class Study(models.Model):
    name = models.CharField(max_length=255, unique=True)
    director = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    creation_date = models.DateTimeField(auto_now=True)
    starting_date = models.DateField()
    expiration_date = models.DateField()

    surveys = models.ManyToManyField(Survey, blank=True)
    microsurvey = models.ManyToManyField(MicroSurvey, blank=True)
    notifications = models.ManyToManyField(Notification, blank=True)
    sensor_requests = models.ManyToManyField(RequestSensors, blank=True)

    def __str__(self):
        return "Name: {0: <10}, Director: {1: <10}".format(self.name, self.director)

    def getIdentifier(self):
        return self.id

    class Meta:
        verbose_name_plural = "03. Studies"


class Participant(models.Model):
    name = models.CharField(max_length=255)
    identifier = models.CharField(max_length=255, default="-", unique=True)
    study = models.ForeignKey(to=Study, on_delete=models.CASCADE, blank=True, null=True)
    registration_date = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, null=True, blank=True)
    restricted_usage = models.BooleanField(default=False, help_text="Remove from admin data export.")

    def __str__(self):
        return "{0: <25} Identifier: {1: <15}".format(self.name, self.identifier)

    class Meta:
        verbose_name_plural = "01. Participant"


# SESSIONS
class Session(models.Model):
    participant = models.ForeignKey(to=Participant, on_delete=models.CASCADE)
    sessionID = models.CharField(max_length=256, blank=True, null=True)
    androidID = models.CharField(max_length=256, blank=True, null=True)
    start = models.CharField(max_length=256, blank=True, null=True)
    ending = models.CharField(max_length=256, blank=True, null=True)
    expiration = models.DateTimeField(blank=True, null=True)
    requested_pre = models.BooleanField(default=False, blank=True, null=True)
    requested_inter = models.BooleanField(default=False, blank=True, null=True)
    requested_post = models.BooleanField(default=False, blank=True, null=True)
    terminated = models.BooleanField(default=False, blank=True, null=True)
    study_name = models.CharField(max_length=255, blank=True, null=True)
    timetable = jsonfield.JSONField(blank=True, null=True)
    target_address = models.CharField(max_length=256,blank=True,null=True)

    def toJson(self):
        serialized = {
            'isSession': True,
            'participant': self.participant.name,
            'sessionID': self.sessionID,
            'androidID': self.androidID,
            'requested_pre': self.requested_pre,
            'requested_inter': self.requested_inter,
            'requested_post': self.requested_post,
            'terminated': self.terminated,
        }
        return json.dumps(serialized)

    def __str__(self):
        q_study = Study.objects.filter(id=int(self.study_name))

        start = "" if self.start == None else self.start
        end = "" if self.ending == None else self.ending

        if q_study:
            return "Study: {1: <15}, Participant{0: <25}, Start: {2: <15}, End: {3: <15}".format(q_study[0].name,
                                                                                                 self.participant.name,
                                                                                                 start,
                                                                                                 end)

        return "StudyID: {1: <15}, Participant{0: <25}, Start: {2: <15}, End: {3: <15}".format(self.study_name,
                                                                                               self.participant.name,
                                                                                               start,
                                                                                               end)

    class Meta:
        verbose_name_plural = "02. Sessions"
