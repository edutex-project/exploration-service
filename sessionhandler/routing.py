from django.urls import re_path

from . import esmconsumer, channelupdateconsumer, observationconsumer

websocket_urlpatterns = [
    re_path(r'ws/socket/(?P<room_name>\w+)/$', esmconsumer.ESMConsumer),
    re_path(r'ws/updateConsumerList/default/', channelupdateconsumer.ChannelUpdateConsumer),
    re_path(r'ws/observe_sockets/observation/', observationconsumer.ObservationConsumer),
]
