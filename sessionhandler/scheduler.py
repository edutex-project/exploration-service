from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.executors.pool import ProcessPoolExecutor, ThreadPoolExecutor
from asgiref.sync import async_to_sync
from apscheduler.jobstores.redis import RedisJobStore
from apscheduler.events import EVENT_JOB_ERROR, EVENT_JOB_EXECUTED
from .scheduler_parser import *
from .scheduler_utils import *
from .models import *
from channels.layers import get_channel_layer
from channels_presence.models import Room, Presence
import uuid
import datetime
import logging
import json
import random
from django.conf import settings

logger = logging.getLogger(__name__)
jobstores = {
    "default": RedisJobStore(host=settings.REDIS_IP, port=settings.REDIS_PORT, db=13)
}
executors = {
    'default': ThreadPoolExecutor(5)
}
job_defaults = {
    "coalesce": False,
    "max_instances": 5
}
scheduler = BackgroundScheduler(jobstores=jobstores, executors=executors,
                                job_defaults=job_defaults)


def add_to_scheduler(trigger, exec, args, function, id, instant=None):
    if trigger == "cron":

        scheduler.add_job(
            function,
            trigger='cron',
            year=str(int(exec[3])),
            month=str(int(exec[4])),
            day=str(int(exec[5])),
            hour=str(int(exec[0])),
            minute=str(int(exec[1])),
            second=str(int(exec[2])),
            args=args,
            id=id,
            misfire_grace_time=60,
            max_instances=1,
        )

    elif trigger == "interval":
        scheduler.add_job(
            function,
            trigger='interval',
            minutes=int(exec),
            args=args,
            id=id,
            misfire_grace_time=10,
            max_instances=1,
            next_run_time=instant
        )


# Registration of Notifications and Surveys
def register_questionnaires(target_address, study):
    # Registering all Notifications
    for notification in Notification.objects.filter(study=study):

        relations = notification.sensorrequest.all()
        if len(relations) > 0:
            continue
        event_id = str(uuid.uuid4())
        register_notification(study, target_address, event_id, notification)

    for survey in Survey.objects.filter(study=study):

        relations = survey.sensorrequest.all()
        if len(relations) > 0:
            continue
        event_id = str(uuid.uuid4())
        register_survey(study, target_address, event_id, survey)


def register_notification(study, target_address, event_id, notification):
    notification_json = notification_parser(notification, study, event_id)
    id = uuid.uuid4.__str__()
    execution_time_offset = get_execution_time(int(notification.offset))
    if notification.type == "single_notification":

        add_to_scheduler('cron',
                         execution_time_offset,
                         [target_address, notification_json, id],
                         notify,
                         notification.name + " " + target_address + " " + str(uuid.uuid4()), )

    elif notification.type == "interval_notification":

        add_to_scheduler('cron',
                         execution_time_offset,
                         [target_address, notification_json, id, notification],
                         offset_interval_registration,
                         notification.name + " " + target_address + " " + str(uuid.uuid4()))


def register_survey(study, target_address, event_id, survey):
    survey_json = survey_parser(survey, study, event_id)

    execution_time = get_execution_time(int(survey.offset))
    id = str(uuid.uuid4())
    if survey.type == "single_survey":
        add_to_scheduler(
            'cron',
            execution_time,
            [target_address, survey_json, id],
            notify,
            survey.name + " " + target_address + " " + str(uuid.uuid4()),
        )

    elif survey.type == "interval_survey":
        add_to_scheduler(
            'cron',
            execution_time,
            [target_address, survey_json, id, survey],
            offset_interval_registration,
            survey.name + " " + target_address + " " + str(uuid.uuid4())
        )


# Registration of MicroSurveys
def register_micro_surveys(target_address, study):
    for micro in MicroSurvey.objects.filter(study=study):
        relations = micro.sensorrequest.all()

        if len(relations) > 0:
            continue

        execution_time = get_execution_time(micro.offset)
        add_to_scheduler(
            'cron',
            execution_time,
            [target_address, study, micro],
            offset_registration_micro_survey,
            str(uuid.uuid4()),
        )


def register_micro(target_address, study, event_id, micro):
    questions = MicroSurveyQuestion.objects.filter(microsurvey=micro)

    # Figuring out Period Segments.
    lifetime = 1 if micro.questionLifetime == "do_remove" else 0
    reminder = 1 if micro.remindOnceAfter == "do_remind" else 0

    schedules = time_segments(micro.schedule, len(questions), micro.questions_schedule, lifetime + reminder)
    sequence = get_sequence(list(questions), micro.display_order_sequence, micro.randomize)

    i = -1
    for question in sequence:
        id = str(uuid.uuid4())
        i += 1
        execution_time = get_execution_time(schedules[i])
        answers = MicroSurveyAnswers.objects.filter(microsurveyquestion=question)
        if len(answers) > 0:
            obj = answers[0]
            answer_possibilities = [obj.first_answer.strip(), obj.second_answer.strip(), obj.third_answer.strip()]

            j_request = micro_parser(micro, study, event_id, lifetime, reminder, question, answer_possibilities, obj)

            add_to_scheduler(
                'cron',
                execution_time,
                [target_address, j_request, id],
                notify,
                question.question + " " + target_address + " " + str(uuid.uuid4())
            )


# Registration of SensorRequests
def register_sensor_requests(target_address, study):
    # Registering all Notifications
    for requests in RequestSensors.objects.filter(study=study):

        event_id = str(uuid.uuid4())

        relations_notification = Notification.objects.filter(sensorrequest=requests)
        relations_surveys = Survey.objects.filter(sensorrequest=requests)
        relations_microsurveys = MicroSurvey.objects.filter(sensorrequest=requests)

        if len(relations_notification) == 1:
            register_notification(study, target_address, event_id, relations_notification[0])

        if len(relations_surveys) == 1:
            register_survey(study, target_address, event_id, relations_surveys[0])

        if len(relations_microsurveys) == 1:
            micro = relations_microsurveys[0]
            execution_time = get_execution_time(micro.offset)
            add_to_scheduler(
                'cron',
                execution_time,
                [target_address, study, micro, event_id],
                offset_registration_micro_survey,
                str(uuid.uuid4()),
            )

        j_request = sensor_request_parser(requests, study, event_id)
        id = str(uuid.uuid4())

        if requests.type == "single_request":
            execution_time = add_minutes_for_cron(int(requests.offset))
            add_to_scheduler(
                'cron',
                execution_time,
                [target_address, j_request, id],
                notify,
                requests.name + " " + target_address + " " + str(uuid.uuid4()),
            )

        elif requests.type == "interval_request":

            offset_execution = get_execution_time(int(requests.offset))
            add_to_scheduler('cron',
                             offset_execution,
                             [target_address, j_request, id, requests],
                             offset_interval_registration,
                             requests.name + " " + target_address + " " + str(uuid.uuid4()))


def offset_interval_registration(target_address, json_obj, id, request):
    add_to_scheduler(
        'interval',
        request.schedule,
        [target_address, json_obj, id],
        notify,
        request.name + " " + target_address + " " + str(uuid.uuid4()),
        datetime.datetime.utcnow()
    )


def offset_registration_micro_survey(target_address, study, micro, event_id=""):
    if event_id == "":
        event_id = str(uuid.uuid4())

    if micro.type == "single_microsurvey":
        register_micro(target_address, study, event_id, micro)
    else:

        add_to_scheduler('interval',
                         micro.schedule,
                         [target_address, study, event_id, micro],
                         register_micro,
                         target_address + " " + str(uuid.uuid4()),
                         datetime.datetime.utcnow())


def notify(target_address, notification_json, id):
    async_to_sync(get_channel_layer().group_send)(target_address,
                                                  {'type': 'chat_message', 'message': notification_json})

    async_to_sync(get_channel_layer().group_send)('observation',
                                                  {'type': 'send.log',
                                                   'message': json.dumps({'destination': 'send-log',
                                                                          'target-address': target_address,
                                                                          'color-type': 'green',
                                                                          'message': notification_json['name'],
                                                                          })}
                                                  )



def start():
    try:
        scheduler.shutdown(wait=False)
    except:
        pass

    try:
        logger.debug("Starting Scheduler")
        scheduler.start()
        prune_scheduled_tasks("", prune_all=True)
        scheduler.add_job(prune_presence, 'interval', seconds=300, misfire_grace_time=15, max_instances=1)
    except KeyboardInterrupt:
        logger.debug("Shutting down Scheduler")
        scheduler.remove_all_jobs()
        scheduler.shutdown(wait=False)

    scheduler.add_listener(listener, EVENT_JOB_ERROR | EVENT_JOB_EXECUTED)


def listener(event):
    # print(event)
    pass


def prune_presence():
    for presence in Presence.objects.all():
        if (datetime.datetime.now() - datetime.timedelta(hours=2) - presence.last_seen.replace(
                tzinfo=None)).total_seconds() > 60:
            prune_scheduled_tasks(presence.room)
            pass
    try:
        Room.objects.prune_presences(age=60)
        Room.objects.prune_rooms()
    except:
        pass


def prune_scheduled_tasks(target_address="", prune_all=False):
    if prune_all:
        # logger.info("Pruning all old jobs.")
        for job in scheduler.get_jobs():
            try:
                job.remove()
                # logger.info("Removed: " + str(job.id))
            except:
                pass
    else:
        # logger.info("Pruning jobs for " + str(target_address))
        for job in scheduler.get_jobs():
            try:
                if job.args[0] == target_address:
                    job.remove()
                    # logger.info("Removed: " + str(job))
                    scheduler.remove_job(job.id)
                    # logger.info("Removed: " + str(job))
            except:
                pass


# Unused
def recover_scheduled_tasks(target_address, tasks):
    for key, value in tasks.items():
        id = str(uuid.uuid4())
        if 'type' in value:
            if value['type'] == 'single':
                if 'execution_time' in value:
                    # TODO Find clever way to fix the reorganisation problem
                    if settings.DEBUG:
                        execution_time = add_seconds_for_cron(value["schedule"])
                    else:
                        execution_time = add_minutes_for_cron(value["schedule"])

                    scheduler.add_job(
                        notify,
                        trigger='cron',
                        year=str(int(execution_time[3])),
                        month=str(int(execution_time[4])),
                        day=str(int(execution_time[5])),
                        hour=str(int(execution_time[0])),
                        minute=str(int(execution_time[1])),
                        second=str(int(execution_time[2])),
                        args=[target_address, value["message"], id],
                        id=" " + target_address + " " + str(uuid.uuid4()),
                        misfire_grace_time=60,
                        max_instances=1,
                    )

            else:

                if settings.DEBUG:
                    scheduler.add_job(
                        notify,
                        trigger='interval',
                        seconds=value['schedule'],
                        args=[target_address, value["message"], id],
                        id=target_address + " " + str(uuid.uuid4()),
                        misfire_grace_time=10,
                        max_instances=1
                    )
                else:
                    scheduler.add_job(
                        notify,
                        trigger='interval',
                        minutes=value['schedule'],
                        args=[target_address, value["message"], id],
                        id=target_address + " " + str(uuid.uuid4()),
                        misfire_grace_time=10,
                        max_instances=1
                    )

