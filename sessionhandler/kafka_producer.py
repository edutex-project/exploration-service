from confluent_kafka import Producer

import socket
import logging
import json
#from bson import json_util

logger = logging.getLogger(__name__)

KAFKA_ESM_TOPIC = "__.de-dipf-edutec-thriller.esm"

KAFKA_ESM_CONFIG = {'bootstrap.servers': "localhost:9092", 'client.id': socket.gethostname()}

class KafkaProducer():

    def __init__(self):
        self.producer = Producer(KAFKA_ESM_CONFIG)
        logger.info("KafkaProducer: Created Kafka producer.")
        print("Init")

    def produce(self, key, value):
        #value = {"name": "Value"}
        value = json.dumps(value).encode('utf-8')
        #key = {"name": "Key"}
        key = json.dumps(key).encode('utf-8')

        print("pre-produce")
        self.producer.produce(topic=KAFKA_ESM_TOPIC, key=key, value=value, callback=self.acked)
        print("post-produce")

        # Wait up to 1 second for events. Callbacks will be invoked during
        # this method call if the message is acknowledged.
        self.producer.poll(0)
        print("post-poll")

        self.producer.flush()
        print("post-flush")

    def acked(self, err, msg):
        """ Called once for each message produced to indicate delivery result.
        Triggered by poll() or flush(). """
        if err is not None:
            logger.info("KafkaProducer: Failed to produce message: %s: %s" % (str(msg), str(err)))
        else:
            key = msg.key().decode('utf-8')
            value = msg.value().decode('utf-8')
            logger.info("KafkaProducer: Successfully produced message with key: '%s', and value '%s'." % (key, value))