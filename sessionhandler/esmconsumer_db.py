from .models import *
import logging
from django.utils.timezone import now
import uuid
from channels.db import database_sync_to_async

logger = logging.getLogger(__name__)


def save_survey(client_response):
    new_survey_entry = DatabaseSurvey(

        name=check_key(client_response, 'name'),
        study=check_key(client_response, 'study'),
        type=check_key(client_response, 'type'),
        schedule=check_key(client_response, 'schedule'),
        sessionID=check_key(client_response, 'sessionID'),
        androidID=check_key(client_response, 'androidID'),
        userID=check_key(client_response, 'userID'),
        event_id=check_key(client_response, "event_id"),
        timestamp_question_asked=check_key(client_response, 'timestamp_question_asked'),
        timestamp_question_answered=check_key(client_response, 'timestamp_question_answered'),
        identifier=check_key(client_response, 'identifier'),
        survey_id=check_key(client_response, 'survey_id'),

    )
    new_survey_entry.save()

    for question_entry in client_response['survey_questions']:

        if "user_answer" in question_entry:
            new_entry = DatabaseSurveyQuestions(

                survey=new_survey_entry,
                display_type=check_key(question_entry, 'display_type'),
                display_order=check_key(question_entry, 'display_order'),
                question=check_key(question_entry, 'question'),
                answers=check_key(question_entry, 'answers'),
                add_answers_allowed=check_key(question_entry, 'add_answers_allowed'),
                user_answer=check_key(question_entry, 'user_answer'),
                survey_question_id=check_key(question_entry, 'question_id'),

            )
            new_entry.save()
        else:
            question_entry["user_answer"] = ""
            new_entry = DatabaseSurveyQuestions(

                survey=new_survey_entry,
                display_type=check_key(question_entry, 'display_type'),
                display_order=check_key(question_entry, 'display_order'),
                question=check_key(question_entry, 'question'),
                answers=check_key(question_entry, 'answers'),
                add_answers_allowed=check_key(question_entry, 'add_answers_allowed'),
                user_answer=check_key(question_entry, 'user_answer'),
                survey_question_id=check_key(question_entry, 'question_id'),

            )
            new_entry.save()
    logger.debug("Saved new Survey instance to database.")


def save_notification(client_response):
    new_entry = DatabaseNotification(
        name=check_key(client_response, 'name'),
        study=check_key(client_response, 'study'),
        type=check_key(client_response, 'type'),
        schedule=check_key(client_response, 'schedule'),
        sessionID=check_key(client_response, 'sessionID'),
        androidID=check_key(client_response, 'androidID'),
        userID=check_key(client_response, 'userID'),
        event_id=check_key(client_response, "event_id"),
        timestamp_question_asked=check_key(client_response, 'timestamp_question_asked'),
        timestamp_question_answered=check_key(client_response, 'timestamp_question_answered'),
        identifier=check_key(client_response, 'identifier'),
        question=check_key(client_response, 'question'),
        answers=check_key(client_response, 'answers'),
        add_answers_allowed=check_key(client_response, 'add_answers_allowed'),
        user_answer=check_key(client_response, 'user_answer'),
        notification_id=check_key(client_response, 'notification_id'),
    )
    new_entry.save()
    logger.debug("Saved new Notification instance to database")


def save_sensor_request(client_response):
    logger.debug("Saved new RequestSensor instance to database.")
    new_entry = DatabaseRequestSensor(
        name=check_key(client_response, "name"),
        userID=check_key(client_response, "userID"),
        sessionID=check_key(client_response, "sessionID"),
        study=check_key(client_response, "study"),
        type=check_key(client_response, "type"),
        schedule=check_key(client_response, "schedule"),
        event_id=check_key(client_response,"event_id"),
        microphone=check_key(client_response, "microphone"),
        wifi=check_key(client_response, "wifi"),
        gps=check_key(client_response, "gps"),
        bluetooth=check_key(client_response, "bluetooth"),
        ambientlight=check_key(client_response, "ambientLight"),
        heartrate=check_key(client_response, "heartrate"),
        heartbeat=check_key(client_response,"heartbeat"),
        accelerometer=check_key(client_response,"accelerometer"),
        temperature=check_key(client_response,"temperature"),
        timestamp_requested=check_key(client_response, "timestamp_requested"),
        sensor=check_key(client_response, "sensor"),
        timestamps=check_key(client_response, "timestamps"),
        values=check_key(client_response, "values"),
        sensorrequest_id=check_key(client_response, "sensorrequest_id")
    )
    new_entry.save()


def save_micro_survey(client_response):
    new_entry = MicroSurveyDatabase(
        name=check_key(client_response, 'name'),
        study=check_key(client_response, 'study'),
        type=check_key(client_response, 'type'),
        schedule=check_key(client_response, 'schedule'),
        sessionID=check_key(client_response, 'sessionID'),
        androidID=check_key(client_response, 'androidID'),
        userID=check_key(client_response, 'userID'),
        event_id=check_key(client_response, "event_id"),
        timestamp_question_asked=check_key(client_response, 'timestamps_question_asked'),
        timestamp_question_answered=check_key(client_response, 'timestamp_question_answered'),
        identifier=check_key(client_response, 'identifier'),
        question=check_key(client_response, 'question'),
        answers=check_key(client_response, 'answers'),
        user_answer=check_key(client_response, 'user_answer'),
        microsurvey_id=check_key(client_response, 'microsurvey_id'),
        microsurvey_question_id=check_key(client_response, 'microsurvey_question_id'),
        microsurvey_answer_id=check_key(client_response,'microsurvey_answers_id'),
    )
    new_entry.save()
    logger.debug("Saved new MicroSurvey instance to database")


def check_key(client_json, key):
    try:
        value = client_json[key]
        return value
    except KeyError as e:
        # logger.info("could not get Key: " + key)
        return "Error not received correct Key."


def create_new_session(participant, is_tmp=False):
    studies = Study.objects.filter(participant=participant)

    new_session = Session(participant=participant,
                          sessionID=uuid.uuid4().__str__(),
                          requested_pre=True,
                          requested_inter=True,
                          start=now(),
                          study_name=studies[0].getIdentifier(),
                          )
    if not is_tmp:
        new_session.save()

    return new_session
